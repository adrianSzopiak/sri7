package edu.pja.sri.lab07.server.handlers;

import java.util.HashMap;
import java.util.Map;

import edu.pja.sri.lab07.OrderItem;
import edu.pja.sri.lab07.ProductCart;
import edu.pja.sri.lab07.server.StorageManager;
import org.apache.thrift.TException;


public class ProductCartHandler implements ProductCart.Iface {

    private final Map<Integer, OrderItem> orders = new HashMap<>();

    @Override
    public void addItem(OrderItem item) throws TException {
        System.out.println("addItem: " + item);
        if (item == null) throw new TException("Cannot add null order item");
        orders.put(item.id, item);
    }

    @Override
    public void deleteItem(int id) throws TException {
//        orders.removeIf(orderItem -> orderItem.id == id);
        orders.remove(id);
    }

    @Override
    public void editItem(OrderItem item) throws TException {
        orders.replace(item.id, item);
    }

    @Override
    public void placeOrder() throws TException {
        System.out.println("PlaceOrder");
        if (orders.isEmpty()) {
            return;
        }
        StorageManager.getInstance().placeOrder(orders);
        orders.clear();
    }
}
