package edu.pja.sri.lab07.server;

import edu.pja.sri.lab07.OrderItem;
import edu.pja.sri.lab07.Product;

import java.util.*;

public class StorageManager {

    private static class Holder {
        private static final StorageManager INSTANCE = new StorageManager();
    }

    private HashMap<Integer, Product> items = new HashMap<>();

    public static StorageManager getInstance() {
        return Holder.INSTANCE;
    }

    private StorageManager() { initStorage();}

    public List<Product> getProducts() {
        return new ArrayList<>(items.values());
    }

    public synchronized boolean placeOrder(Map<Integer, OrderItem> orderList) {
        if (orderList.values().stream().anyMatch(obj -> items.get(obj.id).iloscSztukWMagazynie < obj.ilosc)) {
            return false;
        }
        orderList.values().forEach(orderItem -> items.get(orderItem.id).iloscSztukWMagazynie -= orderItem.ilosc);
        return true;
    }


    private void initStorage() {
        Random r = new Random();
        List<Product> list = new ArrayList<>();
        list.add(new Product(r.nextInt(), "Maslo", 5.0, (short) 10000));
        list.add(new Product(r.nextInt(), "Mleko", 3.0, (short) 10000));
        list.add(new Product(r.nextInt(), "Kakao", 10.0, (short) 10000));
        list.add(new Product(r.nextInt(), "Piwo", 6.5, (short) 10000));
        list.add(new Product(r.nextInt(), "Chleb", 4.0, (short) 10000));
        list.add(new Product(r.nextInt(), "Sok", 3.0, (short) 10000));
        list.add(new Product(r.nextInt(), "WodaNG", 1.5, (short) 10000));
        list.add(new Product(r.nextInt(), "WodaG", 1.5, (short) 10000));
        list.add(new Product(r.nextInt(), "Ser", 20.0, (short) 10000));
        list.add(new Product(r.nextInt(), "Worki na smieci", 5.0, (short) 10000));
        list.add(new Product(r.nextInt(), "Zapiekanka", 3.0, (short) 10000));
        list.add(new Product(r.nextInt(), "HotDog", 2.5, (short) 10000));
        list.forEach(product -> items.put(product.id, product));
    }
}
