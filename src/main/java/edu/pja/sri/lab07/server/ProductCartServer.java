package edu.pja.sri.lab07.server;

import edu.pja.sri.lab07.ProductCart;
import edu.pja.sri.lab07.ProductManager;
import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

import edu.pja.sri.lab07.server.handlers.ProductCartHandler;
import edu.pja.sri.lab07.server.handlers.ProductManagerHandler;

// Generated code

public class ProductCartServer {

	  public static ProductCartHandler handlerCart;
	  public static ProductManagerHandler handlerManager;

	  public static ProductCart.Processor processorCart;
	  public static ProductManager.Processor processorManager;

	  public static void main(String [] args) {
	  	StorageManager.getInstance();
	      handlerCart = new ProductCartHandler();
	      processorCart = new ProductCart.Processor(handlerCart);
	      handlerManager = new ProductManagerHandler();
	      processorManager = new ProductManager.Processor(handlerManager);
	      new Thread(() -> multiple(processorCart, processorManager)).start();
//	      Runnable simple = () -> simple(processorCart, processorManager);
//
//	      new Thread(simple).start();
	  }

	  public static void multiple(ProductCart.Processor productCartProcessor, ProductManager.Processor productManagerProcessor) {
		  try {
			  TMultiplexedProcessor processor = new TMultiplexedProcessor();
			  TServerTransport t = new TServerSocket(9090);
			  TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(t).processor(processor));
			  processor.registerProcessor("ProductCart", productCartProcessor);
			  processor.registerProcessor("ProductManager", productManagerProcessor);

			  System.out.println("starting server");
			  server.serve();
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
	  }

	  public static void simple(ProductCart.Processor productCartProcessor, ProductManager.Processor productManagerProcessor) {
	    try {
	      TServerTransport serverTransport = new TServerSocket(9090);
	      TServer server = new TSimpleServer(new Args(serverTransport).processor(processorCart));

	      System.out.println("Starting the simple server...");
	      server.serve();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	  }
	 
	}