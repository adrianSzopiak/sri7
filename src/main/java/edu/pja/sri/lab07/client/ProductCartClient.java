package edu.pja.sri.lab07.client;

import edu.pja.sri.lab07.OrderItem;
import edu.pja.sri.lab07.Product;
import edu.pja.sri.lab07.ProductCart;
import edu.pja.sri.lab07.ProductManager;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import java.util.List;

public class ProductCartClient {

    private static ProductManager.Client productManagerclient;
    private static ProductCart.Client cartClient;

    public static void main(String[] args) {
        try {
            TTransport transport = new TSocket("localhost", 9090);
            transport.open();

            TProtocol protocol = new TBinaryProtocol(transport);
//		      initSimple(protocol);
            initMulti(protocol);
            perform(productManagerclient, cartClient);

            transport.close();
        } catch (TException x) {
            x.printStackTrace();
        }
    }

    private static void initMulti(TProtocol protocol) {
        TMultiplexedProtocol mp = new TMultiplexedProtocol(protocol, "ProductManager");
        productManagerclient = new ProductManager.Client(mp);
        TMultiplexedProtocol mp2 = new TMultiplexedProtocol(protocol, "ProductCart");
        cartClient = new ProductCart.Client(mp2);
    }

    private static void initSimple(TProtocol protocol) {
        productManagerclient = new ProductManager.Client(protocol);
        cartClient = new ProductCart.Client(protocol);
    }

    private static void perform(ProductManager.Client productManagerclient, ProductCart.Client cartClient) throws TException {

        List<Product> allProducts = productManagerclient.getAllProducts();
        OrderItem it1 = new OrderItem(allProducts.get(0).getId(), (short) 100);
        cartClient.addItem(it1);
        OrderItem it2 = new OrderItem(allProducts.get(1).getId(), (short) 10);
        cartClient.addItem(it2);
        OrderItem it3 = new OrderItem(allProducts.get(5).getId(), (short) 99);
        cartClient.addItem(it3);
        cartClient.deleteItem(allProducts.get(1).id);
        cartClient.placeOrder();
        System.out.println(productManagerclient.getAllProducts().toString());
    }
}
