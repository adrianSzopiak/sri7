namespace java edu.pja.sri.lab07

struct Product {
    1: i32 id,
    2: string nazwa,
    3: double cena,
    4: i16 iloscSztukWMagazynie
}

struct OrderItem {
    1: i32 id,
    2: i16 ilosc
}

service ProductManager {
    list<Product> getAllProducts()
}

service ProductCart {
    void addItem(1: OrderItem item),
    void deleteItem(1: i32 id),
    void editItem(1: OrderItem item),
    void placeOrder()
}
